---
title: isolaet
date: 2021-03-20
artwork: /img/isolaet_logo.webp
artworkAuthor: Liam Fitzpatrick
releaseType: album
---

> The flagship release for isolaet.

Features the following tracks:

Awaken to Freedom - Uncertain - Escape - Shelter - Embrace - Safe - The Return
