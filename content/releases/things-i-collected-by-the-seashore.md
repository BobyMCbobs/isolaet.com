---
title: things i collected from the seashore
date: 2024-10-08
artwork: /img/isolaet_things_i_collected_from_the_seashore.png
artworkAuthor: Liam Fitzpatrick, Caleb Woodbine
releaseType: album
---

Featuring the following tracks:

Squirrel - Orange - Set Course - Ponder - Your Dog is Cool - Solstice - Dream (2024 remaster) - Aquaitic Friends - Untamed Beast - Nice - Cityscape - Coil (2024 remaster) - UNKNOWN ENTITY
