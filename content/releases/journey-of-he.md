---
title: Journey of He
date: 2023-03-26
artwork: /img/isolaet_journey_of_he.webp
artworkAuthor: Liam Fitzpatrick
releaseType: album
---

Featuring the following tracks:

Emergent - Era - Abyss - Crono - Satellite - Serenity - Notion - Floating - Undone - Enduring
