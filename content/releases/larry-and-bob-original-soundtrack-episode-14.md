---
title: "Larry and Bob Original Soundtrack: Episode 14 (isolaet)"
date: 2024-10-27
artwork: "/img/isolaet_larry_and_bob_original_soundtrack_episode_14_isolaet.png"
artworkAuthor: Liam Fitzpatrick
releaseType: soundtrack
---

Featuring 5 tracks from [AbsolutelyCrayCray's](https://absolutelycraycray.com/) [Larry and Bob](https://absolutelycraycray.com/youtube) web series, including some ones previous unheard.
