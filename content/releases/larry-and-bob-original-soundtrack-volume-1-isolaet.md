---
title: "Larry and Bob Original Soundtrack: Volume 1 (isolaet)"
date: 2024-10-19
artwork: "/img/isolaet_larry_and_bob_original_soundtrack_volume_1_isolaet.png"
artworkAuthor: Liam Fitzpatrick
releaseType: soundtrack
---

Featuring 29 tracks from [AbsolutelyCrayCray's](https://absolutelycraycray.com/) [Larry and Bob](https://absolutelycraycray.com/youtube) web series.
