---
title: Dream
date: 2023-05-09
artwork: /img/isolaet_dream.webp
artworkAuthor: Caleb Woodbine
releaseType: single
---

> I want you to see the world in wonder.
