#!/bin/sh -x

set -o errexit
set -o nounset
set -o pipefail

cd "$(git rev-parse --show-toplevel)" || exit 0

podman run --rm --network=host \
    -v "$PWD:$PWD" --workdir "$PWD" \
    alpine:edge \
      sh -c "
apk add --no-cache $(cat .alpine.pkgs) ;
git config --global --add safe.directory "$PWD" ;
./hack/publish.sh "${@:-}"
"
